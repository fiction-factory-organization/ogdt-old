/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-05-22T10:53:31+02:00
 * @Project: OGDT
 * @Filename: gulpfile.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-09-05T22:59:33+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var terser = require('gulp-terser');

gulp.task('all', ['styles', 'js', 'fa', 'icons', 'animate','images']);

gulp.task('styles', function() {
    gulp.src('assets/src/scss/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('static/css/'))
});

gulp.task('js', function() {
    gulp.src(
      [
        'assets/src/js/modules/workspace.js',
        'assets/src/js/modules/animate.module.js',
        'assets/src/js/modules/const.module.js',
        'assets/src/js/modules/ajax.module.js',
        'assets/src/js/modules/buttons.module.js',
        'assets/src/js/modules/dropdown.module.js',
        'assets/src/js/modules/modals.module.js',
        'assets/src/js/modules/clipboard.module.js',
        'assets/src/js/modules/form.module.js',
        'assets/src/js/modules/messages.module.js',
      ]
    )
    .pipe(concat('premods.js'))
    .pipe(gulp.dest('static/js/'));

    gulp.src(
      [
        'assets/src/js/*',
        'assets/src/js/modules/tab.module.js',
        'assets/src/js/modules/workgroupSelector.module.js',
        'assets/src/js/modules/projectSelector.module.js',
        'assets/src/js/modules/workplaceSelector.module.js',
        'assets/src/js/modules/transferSelector.module.js',
      ]
    )
    .pipe(concat('modules.js'))
    .pipe(gulp.dest('static/js/'));

    gulp.src(
      [
        'assets/src/js/adminPanel/*',
        'assets/src/js/modules/adminPanel.module.js',
      ]
    )
    .pipe(concat('admin-modules.js'))
    .pipe(gulp.dest('static/js/'));

    gulp.src(
      [
        'assets/src/js/modules/profilePanel.module.js',
      ]
    )
    .pipe(concat('profile-modules.js'))
    .pipe(gulp.dest('static/js/'));

    gulp.src(
      [
        'node_modules/jquery/jquery.min.js',
        'node_modules/jquery-mousewheel/jquery.mousewheel.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'assets/src/js/addons/holder.js',
        'assets/src/js/addons/jquery.transfer.js',
        'node_modules/bootbox/dist/bootbox.all.min.js',
        'node_modules/@fortawesome/fontawesome-free/js/all.min.js',
        'node_modules/animate.css/animate.min.js',
        'node_modules/js-cookie/src/js.cookie.js',
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-select/js/dataTables.select.js',
        'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
        'node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.js',
        'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
        'node_modules/bootstrap-select/dist/js/bootstrap-select.js',
      ]
    )
    .pipe(concat('dependencies.js'))
    .pipe(gulp.dest('static/js/'));
});

gulp.task('images', function() {
    gulp.src('assets/src/img/*')
    .pipe(gulp.dest('static/img'));
    gulp.src('assets/src/favicon.png')
    .pipe(gulp.dest('static/'));
});

gulp.task('fa', function() {
    gulp.src('node_modules/@fortawesome/fontawesome-free/scss/fontawesome.scss')
    .pipe(sass())
    .pipe(gulp.dest('static/css'))
});

gulp.task('icons', function() {
    gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest('static/webfonts/'));
});

gulp.task('animate', function() {
    gulp.src('node_modules/animate.css/animate.min.css')
    .pipe(gulp.dest('static/css/'));
});
