# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:31+02:00
# @Project: OGDT
# @Filename: serializers.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-09T17:18:09+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from rest_framework import serializers
from .models import Profile, Workplace, Workgroup, Project
from django.contrib.auth.models import User

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id',
            'user',
            'bio',
            'firstname',
            'lastname',
            'email',
            'location',
            'time_zone',
            'birth_date',
            'workplaces',
            'fworkplaces',
            'modified_at',
            'created_at',
            )

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            # 'password',
            'groups',
            'user_permissions',
            'is_staff',
            'is_active',
            'is_superuser',
            'last_login',
            'date_joined',
            )

class WorkplaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workplace
        fields = (
            'id',
            'label',
            'owner_type',
            'time_zone',
            'url_prefix',
            'created_at',
            'modified_at',
            )

class WorkgroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workgroup
        fields = (
            'id',
            'label',
            'users',
            'managers',
            'workplace',
            'created_at',
            'modified_at',
            )

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = (
            'id',
            'label',
            'workgroups',
            'workplace',
            'created_at',
            'modified_at',
            )
