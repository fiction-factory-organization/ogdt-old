# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:31+02:00
# @Project: OGDT
# @Filename: models.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-09T15:52:54+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from django.urls import reverse

import random
import string
import datetime
import hashlib

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

def randomStringDigits(stringLength=6):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))

def generateUniqueForeignId(label):
    """Generate unique ID based on label, random string and current date """
    hashSource = label+'_'+datetime.datetime.now().strftime("%d-%b-%Y_(%H:%M:%S.%f)")
    hash = hashlib.sha256(hashSource.encode("utf-8"))
    return hash.hexdigest()

# Const

OWNER_TYPES = [
    ("company","company"),
    ("organization","organization"),
    ("individual","individual"),
]

TIMEZONES = [
    ("Workplace","Workplace"),
    ("UTC-11","UTC-11"),("UTC-10","UTC-10"),("UTC-9","UTC-9"),("UTC-8","UTC-8"),
    ("UTC-7","UTC-7"),("UTC-6","UTC-6"),("UTC-5","UTC-5"),("UTC-4","UTC-4"),("UTC-3","UTC-3"),
    ("UTC-2","UTC-2"),("UTC-1","UTC-1"),

    ("UTC","UTC"),

    ("UTC+1","UTC+1"),("UTC+2","UTC+2"),("UTC+3","UTC+3"),("UTC+4","UTC+4"),
    ("UTC+5","UTC+5"),("UTC+6","UTC+6"),("UTC+7","UTC+7"),("UTC+8","UTC+8"),("UTC+9","UTC+9"),
    ("UTC+10","UTC+10"),("UTC+11","UTC+11"),("UTC+12","UTC+12"),
]

class Workplace(models.Model):

    class Meta:
        verbose_name = "workplace"
        verbose_name_plural = "workplaces"

    label = models.CharField(max_length=150, null=False, blank=False)
    owner_type = models.CharField(max_length=150, choices=OWNER_TYPES, blank=False, null=False)
    url_prefix = models.CharField(max_length=150, null=False, blank=False)
    unique_foreign_id = models.CharField(max_length=255, null=False, blank=False, default='None')
    time_zone = models.CharField(default="UTC",max_length=50,choices=TIMEZONES)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

    @staticmethod
    def get_ownertype_choices():
        return OWNER_TYPES

    @staticmethod
    def get_timezone_choices():
        return TIMEZONES

    def save(self, *args, **kwargs):
        if not self.label:
            self.label = 'Workplace_'+randomStringDigits(32)
            self.unique_foreign_id = generateUniqueForeignId(self.label)
        else:
            self.unique_foreign_id = generateUniqueForeignId(self.label+'_'+randomStringDigits(32))
        super().save(*args, **kwargs)

class ForeignWorkplace(models.Model):

    class Meta:
        verbose_name = "foreign workplace"
        verbose_name_plural = "foreign workplaces"

    label = models.CharField(max_length=150, null=False, blank=True)
    owner_type = models.CharField(max_length=150, choices=OWNER_TYPES, blank=True, null=False)
    url_prefix = models.CharField(max_length=150, null=False, blank=True)
    unique_foreign_id = models.CharField(max_length=255, null=False, blank=False, default='None')

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    remote_addr = models.CharField(max_length=150, null=False, blank=False)
    is_valid = models.BooleanField(null=False, default=False)

    def __str__(self):
        return self.label+' @ '+self.remote_addr

    def validate(self):
        # TODO: Implement Foreign validation
        pass
    @staticmethod
    def get_ownertype_choices():
        return OWNER_TYPES

    @staticmethod
    def get_timezone_choices():
        return TIMEZONES

    def save(self, *args, **kwargs):
        # if not self.label:
        #     self.label = 'Workplace_'+randomStringDigits(32)
        #     self.unique_foreign_id = generateUniqueForeignId(self.label)
        # else:
        #     self.unique_foreign_id = generateUniqueForeignId(self.label+'_'+randomStringDigits(32))
        super().save(*args, **kwargs)

class Profile(models.Model):

    class Meta:
        verbose_name = "profile"
        verbose_name_plural = "profiles"
        permissions = (
            ('edit_self','Edit self details'),
        )

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False)
    dname = models.CharField(max_length=150, blank=False)
    bio = models.TextField(max_length=500, blank=True)
    firstname = models.CharField(max_length=150, blank=True)
    lastname = models.CharField(max_length=150, blank=True)
    email = models.EmailField(max_length=254, blank=True)
    workplaces = models.ManyToManyField(Workplace, blank=True)
    fworkplaces = models.ManyToManyField(ForeignWorkplace, blank=True)
    location = models.CharField(max_length=150, blank=True)
    time_zone = models.CharField(default="Workplace",max_length=50,choices=TIMEZONES)
    birth_date = models.DateField(null=True, blank=True)
    can_auth = models.BooleanField(blank=False, default=True)

    modified_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.__str__()

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance,dname=instance.username)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def get_timezone_choices(self):
        return TIMEZONES

class Cluster(models.Model):

    class Meta:
        verbose_name = "cluster"
        verbose_name_plural = "clusters"

    label = models.CharField(max_length=150, null=False, blank=False)
    workplace = models.OneToOneField(Workplace, null=False, on_delete=models.CASCADE)
    foreign_workplaces = models.ManyToManyField(ForeignWorkplace, blank=False)

    def __str__(self):
        return self.label

class Workgroup(models.Model):

    class Meta:
        verbose_name = "workgroup"
        verbose_name_plural = "workgroups"

    label = models.CharField(max_length=150,blank=False,null=False)
    users = models.ManyToManyField(Profile, blank=True, related_name='users')
    managers = models.ManyToManyField(Profile, blank=True, related_name='managers')
    workplace = models.ForeignKey(Workplace, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

class Project(models.Model):

    class Meta:
        verbose_name = "project"
        verbose_name_plural = "projects"

    label = models.CharField(max_length=150,blank=False,null=False)
    workgroups = models.ManyToManyField(Workgroup)
    workplace = models.ForeignKey(Workplace, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

class App(models.Model):

    class Meta:
        verbose_name = "app"
        verbose_name_plural = "apps"

    name = models.CharField(max_length=150,blank=False,null=False)
    description = models.CharField(max_length=150,blank=False,null=False)
    url = models.CharField(max_length=150,blank=False,null=False)
    version = models.CharField(max_length=50,blank=False,null=False)
    enabled = models.BooleanField(default=False)

    installed_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})
