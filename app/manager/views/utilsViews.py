# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:32+02:00
# @Project: OGDT
# @Filename: utilsViews.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-09-05T15:44:55+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

import logging
import logging.config

import json

import django.middleware.csrf
from ..common.decorators import ajax_required

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.template import Context, Template, loader, RequestContext
from django.contrib import messages

from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated

from ..serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer
from ..models import Profile, Workplace, Workgroup, Project

# Utils views
@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workgroupRegister(request):
    if request.session.get('workgroup') and request.POST.get('change') is None :
        return JsonResponse({'registered':request.session.get('workgroup_label')},status=200)
    if 'workgroup' in request.POST:
        request.session['workgroup']=request.POST.get('workgroup')
        workgroup = Workgroup.objects.get(id=request.POST.get('workgroup'))
        request.session['workgroup_label']=workgroup.label
        return HttpResponse('',status=200)
    else:
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workgroupUnregister(request):
    if request.session.get('workgroup'):
        request.session.modified = True
        del request.session['workgroup']
        del request.session['workgroup_label']

    return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def projectRegister(request):
    if request.session.get('project') and request.POST.get('change') is None :
        return JsonResponse({'registered':request.session.get('project_label')},status=200)
    if 'project' in request.POST:
        request.session['project']=request.POST.get('project')
        request.session['project_label']=request.POST.get('project_label')
        return HttpResponse('',status=200)
    else:
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def projectUnregister(request):
    if request.session.get('project'):
        request.session.modified = True
        del request.session['project']

    return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def saveTab(request):
    if not request.session.get('tabs'):
        request.session['tabs'] = {}
    request.session.modified = True
    request.session['tabs'][request.POST.get('id')] = {
            'title':request.POST.get('title'),
            'url':request.POST.get('url'),
            'app':request.POST.get('app'),
            'appdata':request.POST.get('appdata') or '',
        }
    return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def delTab(request):
    if request.session.get('tabs', None):
        if request.POST.get('id') == '*':
            for tab in request.session['tabs']:
              if request.session['tabs'][tab]['app'] not in ['workplaces','admin']:
                del request.session['tabs'][tab]
        else:
            del request.session['tabs'][request.POST.get('id')]
    request.session.modified = True
    return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def loadTabs(request):
    if request.session.get('tabs', None):
        if request.session.get('_current_tab'):
            return JsonResponse({'tabs':request.session.get('tabs'),'current_tab':request.session.get('_current_tab')})
        return JsonResponse({'tabs':request.session.get('tabs')})
    else:
        return JsonResponse({})

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def checkObject(request):
    # USER FIELDS
    if 'user.username' in request.POST:
        if User.objects.filter(username=request.POST.get('user.username')).exists():
            error = {
                        'error':
                            {
                                'message':'Username '+request.POST.get('user.username')+' already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        return HttpResponse('',status='200')
    # PROFILE FIELDS
    elif 'profile.email' in request.POST:
        if Profile.objects.filter(email=request.POST.get('profile.email')).exists():
            error = {
                        'error':
                            {
                                'message':'Email '+request.POST.get('profile.email')+' already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        return HttpResponse('',status='200')
    # WORKPLACE FIELDS
    elif 'workplace.label' in request.POST:
        if Workplace.objects.filter(label=request.POST.get('workplace.label')).exists():
            error = {
                        'error':
                            {
                                'message':'Label '+request.POST.get('workplace.label')+' already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        return HttpResponse('',status='200')
    elif 'workplace.urlPrefix' in request.POST:
        if Workplace.objects.filter(url_prefix=request.POST.get('workplace.urlPrefix')).exists():
            error = {
                        'error':
                            {
                                'message':'Url Prefix '+request.POST.get('workplace.urlPrefix')+' already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        return HttpResponse('',status='200')
    # WORKGROUP FIELDS
    elif 'workgroup.label' in request.POST:
          if Workgroup.objects.filter(label=request.POST.get('workgroup.label')).exists():
              error = {
                          'error':
                              {
                                  'message':'Label '+request.POST.get('workgroup.label')+' already in use',
                              }
                      }
              return JsonResponse(error,status='200')
          return HttpResponse('',status='200')
    # ERROR
    else:
        return JsonResponse('',status='200')
