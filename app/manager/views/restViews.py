# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:32+02:00
# @Project: OGDT
# @Filename: restViews.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-07-30T18:07:59+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

import logging
import logging.config

import django.middleware.csrf

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from ..serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer, UserSerializer
from ..models import Profile, Workplace, Workgroup, Project
from django.contrib.auth.models import User

# REST API

class ProfileView(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if 'select' in self.request.GET:
            if(self.request.user.id):
                userId = self.request.user.id
                return Profile.objects.filter(users=userId,workplace=workplaceId)
        else:
            return Profile.objects.all()

class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if 'select' in self.request.GET:
            if(self.request.user.id):
                userId = self.request.user.id
                return User.objects.filter(id=userId)
        else:
            return User.objects.all()

class WorkplaceView(viewsets.ModelViewSet):
    serializer_class = WorkplaceSerializer
    queryset = Workplace.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if 'select' in self.request.GET:
            pass
            # if(self.request.user.id):
            #     userId = self.request.user.id
            #     return Workplace.objects.filter(users=userId,workplace=workplaceId)
        else:
            return Workplace.objects.all()

class WorkgroupView(viewsets.ModelViewSet):
    serializer_class = WorkgroupSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if 'select' in self.request.GET:
            if(self.request.user.id and self.request.session.get('workplace', None) != None):
                userId = self.request.user.id
                workplaceId = self.request.session['workplace']
                return Workgroup.objects.filter(users=userId,workplace=workplaceId)
        else:
            return Workgroup.objects.all()

class ProjectView(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):

        if 'select' in self.request.GET:
            if(self.request.user.id and
                self.request.session.get('workplace', None) != None and
                self.request.session.get('workgroup', None) != None):

                workgroupId = self.request.session['workgroup']
                workplaceId = self.request.session['workplace']
                return Project.objects.filter(
                    workplace=workplaceId,
                    workgroups=workgroupId
                )

        else:
            return Project.objects.all()
