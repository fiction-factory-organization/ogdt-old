# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:32+02:00
# @Project: OGDT
# @Filename: embededViews.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-09-05T18:27:55+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

import django.middleware.csrf
from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib import messages
from django.template import Context, Template, loader, RequestContext
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from manager.serializers import ProfileSerializer, WorkplaceSerializer, WorkgroupSerializer, ProjectSerializer
from manager.models import Profile, Workplace, Workgroup, Project, App, ForeignWorkplace, Cluster
from django.contrib.auth.models import User

# Embeded views

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def workplaceSelector(request):
    if 'workplace' not in request.POST:
        workplaces = []
        fworkplaces = []

        profile = Profile.objects.get(user_id = request.user.id)
        workplaces = profile.workplaces.all()

        for cluster in Cluster.objects.filter(workplace__id = request.session.get('workplace')):
            for wp in cluster.foreign_workplaces.filter(is_valid = True):
                fworkplaces.append(wp)

        context = {'workplaces':workplaces,'fworkplaces':fworkplaces}
        template = loader.get_template('manager/WorkplaceSelector.html')
        return HttpResponse(template.render(context, request))
    else:
        request.session['workplace']=request.POST.get('workplace')
        return HttpResponse('',status=200)

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def appSelector(request):
    workplace = request.session.get('workplace', None)
    queryset = App.objects.all()
    context = {'apps':queryset,'workplace':workplace}
    template = loader.get_template('manager/AppSelector.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminPanel(request):
    wpOwnerTypes = []
    wpTimeZones = []
    workgroups = []
    ownerTypes = Workplace.get_ownertype_choices()
    timeZones = Workplace.get_timezone_choices()
    workgroups = Workgroup.objects.all()
    for ownerType in ownerTypes:
      wpOwnerTypes.append(ownerType[0])
    for timeZone in timeZones:
      wpTimeZones.append(timeZone[0])
    workplaces = Workplace.objects.all().order_by('id');
    context = {'wpOwnerTypes':wpOwnerTypes,'wpTimeZones':wpTimeZones,'workplaces':workplaces,'workgroups':workgroups}
    template = loader.get_template('manager/Admin.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def profilePanel(request):
    currentUser = request.user
    profile = Profile.objects.get(
        user_id = currentUser.id
    )
    user = User.objects.get(
        id = currentUser.id
    )
    context = {'user':user,'profile':profile}
    template = loader.get_template('manager/Profile.html')
    return HttpResponse(template.render(context, request))
