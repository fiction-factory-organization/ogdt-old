#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

from django.template import loader
from django.http import HttpResponse

from manager.models import Profile
from django.contrib.auth.models import User

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

from django.core import serializers

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def profileDetails(request):
    currentUser = request.user
    profile = Profile.objects.get(
        user_id = currentUser.id
    )
    queryset = Profile.objects.filter(
        user_id = currentUser.id
    )
    context = {'profile':profile,'profile_dump':serializers.serialize('python',queryset)}
    template = loader.get_template('manager/Profile/Details.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def profileSecurity(request):
    currentUser = request.user
    profile = Profile.objects.get(
        user_id = currentUser.id
    )
    queryset = Profile.objects.filter(
        user_id = currentUser.id
    )
    context = {'profile':profile,'profile_dump':serializers.serialize('python',queryset)}
    template = loader.get_template('manager/Profile/Security.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def profilePreferences(request):
    currentUser = request.user
    profile = Profile.objects.get(
        user_id = currentUser.id
    )
    queryset = Profile.objects.filter(
        user_id = currentUser.id
    )
    context = {'profile':profile,'profile_dump':serializers.serialize('python',queryset)}
    template = loader.get_template('manager/Profile/Preferences.html')
    return HttpResponse(template.render(context, request))
