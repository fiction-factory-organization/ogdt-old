#
# OpenGDToolbox : collaborative web app made for game creators to simplify game
# design work flow and collaboration.
# Copyright (C) 2019 Thibaud FAURIE
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

from django.template import loader
from django.http import HttpResponse, JsonResponse

from manager.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import render, redirect

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def changeUsername(request):
    if 'username' in request.POST:
        currentUser = request.user
        newusername = request.POST.get('username')
        if User.objects.filter(username=newusername).exists():
            error = {
                        'error':
                            {
                                'message':'Username is already in use',
                            }
                    }
            return JsonResponse(error,status='500')
        currentUser.username = newusername
        currentUser.save()
        return HttpResponse('',status='200')
    else:
        return HttpResponse('',status='500')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def changePassword(request):
    if 'password' in request.POST:
        currentUser = request.user
        if request.POST.get('password') == '':
            error = {
                        'error':
                            {
                                'message':'New password can\'t be empty',
                            }
                    }
            return JsonResponse(error,status='500')

        currentUser.set_password(request.POST.get('password'))
        currentUser.save()
        update_session_auth_hash(request, request.user)
        return HttpResponse('',status='200')
    else:
        error = {
                    'warning':
                        {
                            'message':'Please provide a new password',
                        }
                }
        return JsonResponse(error,status='500')
