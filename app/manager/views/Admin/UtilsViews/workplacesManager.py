# @Author: Thibaud FAURIE
# @Date:   2019-08-09T14:55:31+02:00
# @Project: OGDT
# @Filename: workplacesManager.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-09-05T15:34:15+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.template import loader
from django.http import HttpResponse, JsonResponse

from manager.models import Profile, Workplace
from django.contrib.auth.models import User

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

import re

# WORKPLACES MANAGER
@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def addWorkplace(request):
    if 'label' in request.POST:
        # get label from POST
        label = request.POST.get('label')
        # check if workplaces are not already using this label
        if Workplace.objects.filter(label=label).exists():
            error = {
                        'error':
                            {
                                'message':'Workplace label "'+label+'" already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        else:
            for value in ['owner_type','url_prefix','time_zone']:
                # check if all compulsory values are parsed
                if value not in request.POST:
                    error = {
                                'error':
                                    {
                                        'message':'Some properties are missing',
                                    }
                            }
                    return JsonResponse(error,status='200')
                # check if all compulsory values are not empty
                elif request.POST.get(value) == '':
                    error = {
                                'error':
                                    {
                                        'message':'Some properties are empty',
                                    }
                            }
                    return JsonResponse(error,status='200')

                # check if owner type is valid
                if value == 'owner_type':
                    if (request.POST.get(value),request.POST.get(value)) not in Workplace.get_ownertype_choices():
                      error = {
                                  'error':
                                      {
                                          'message':'Owner type is invalid',
                                      }
                              }
                      return JsonResponse(error,status='200')

                # check if url_prefix is valid
                if value == 'url_prefix':
                    pattern = re.compile("([a-z])\w+")
                    print(pattern.match(request.POST.get(value)))
                    if pattern.match(request.POST.get(value)) == None:
                      error = {
                                  'error':
                                      {
                                          'message':'Url prefix is invalid',
                                      }
                              }
                      return JsonResponse(error,status='200')

                # check if owner type is valid
                if value == 'time_zone':
                    if (request.POST.get(value),request.POST.get(value)) not in Workplace.get_timezone_choices():
                      error = {
                                  'error':
                                      {
                                          'message':'Timezone is invalid',
                                      }
                              }
                      return JsonResponse(error,status='200')

            owner_type = request.POST.get('owner_type')
            url_prefix = request.POST.get('url_prefix')
            time_zone = request.POST.get('time_zone')
            workplace = Workplace.objects.create(label=label, owner_type=owner_type, url_prefix=url_prefix, time_zone=time_zone)
            workplace.save()
            return HttpResponse('',status='200')
    else:
        return HttpResponse('',status='500')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def delWorkplace(request):
    if 'id' in request.POST:
        workplace = Workplace.objects.get(id=request.POST.get('id'))
        workplace.delete()
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted workplace '+workplace.label+'.',
                        }
                }
        return JsonResponse(data,status='200')
    elif 'id_list' in request.POST:
        deleted = 0
        notDeleted = 0
        for id in request.POST.get('id_list'):
            workplace = Workplace.objects.get(id=id)
            workplace.delete()
            deleted +=1
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted '+deleted+' workplace'+('s' if deleted > 1 else '')+'.',
                        },
                }
        return JsonResponse(data,status='200')
    else:
        return HttpResponse('',status='500')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def editWorkplace(request):
    data = {'error':{'message':'','reasons':[]},'success':{'message':''}}
    error = False
    if 'id' in request.POST and request.POST.get('id') != '':
        id = request.POST.get('id')
        workplace = Workplace.objects.filter(id=id);
    else:
        data['error']['message'] = 'Failed to edit workplace'
        data['error']['reasons'].append('Reason: No object id has been specified')
        return JsonResponse(data,status='200')
    if 'label' in request.POST and request.POST.get('label') != '':
        if Workplace.objects.filter(label=request.POST.get('label')).exists():
            error = True
            data['error']['reasons'].append('Reason: Specified label is already in use')
        else:
            workplace.update(label=request.POST.get('label'))
    if 'url_prefix' in request.POST and request.POST.get('url_prefix') != '':
        pattern = re.compile("([a-z])\w+")
        if pattern.match(request.POST.get('url_prefix')) == None:
            error = True
            data['error']['reasons'].append('Reason: Url prefix is invalid')
        elif Workplace.objects.filter(url_prefix=request.POST.get('url_prefix')).exists():
            error = True
            data['error']['reasons'].append('Reason: Url prefix is already in use')
        else:
            workplace.update(url_prefix=request.POST.get('url_prefix'))
    if 'time_zone' in request.POST and (request.POST.get('time_zone'),request.POST.get('time_zone')) in Workplace.get_timezone_choices():
        workplace.update(time_zone=request.POST.get('time_zone'))
    if 'owner_type' in request.POST and (request.POST.get('owner_type'),request.POST.get('owner_type')) in Workplace.get_ownertype_choices():
        workplace.update(owner_type=request.POST.get('owner_type'))
    if error == True:
        data['error']['message'] = 'Failed to edit workplace'
    else:
        data['success']['message'] = 'Successfuly edit workplace'
    return JsonResponse(data,status='200')
