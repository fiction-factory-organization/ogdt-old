# @Author: Thibaud FAURIE
# @Date:   2019-08-09T14:52:29+02:00
# @Project: OGDT
# @Filename: usersManager.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-09T15:46:39+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.template import loader
from django.http import HttpResponse, JsonResponse

from manager.models import Profile, Workplace
from django.contrib.auth.models import User

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

# USERS MANAGER
@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def addUser(request):
    if 'username' in request.POST:
        username = request.POST.get('username')
        if User.objects.filter(username=username).exists():
            error = {
                        'error':
                            {
                                'message':'User '+username+' already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        else:
            for value in ['username','email','password']:
                if value not in request.POST:
                    error = {
                                'error':
                                    {
                                        'message':'Some properties are missing',
                                    }
                            }
                    return JsonResponse(error,status='200')
                elif request.POST.get(value) == '':
                    error = {
                                'error':
                                    {
                                        'message':'Some properties are empty',
                                    }
                            }
                    return JsonResponse(error,status='200')
                #VERIFS HERE
            password = request.POST.get('password')
            email = request.POST.get('email')
            user = User.objects.create_user(username, email, password)
            user.save()
            profile = Profile.objects.get(user_id=user.id)
            workplaceToAdd = Workplace.objects.get(id=request.session['workplace'])
            profile.workplaces.add(workplaceToAdd)
            if 'firstname' in request.POST and request.POST.get('firstname') != '':
                Profile.objects.filter(user_id=user.id).update(firstname=request.POST.get('firstname'))
            if 'lastname' in request.POST and request.POST.get('lastname') != '':
                Profile.objects.filter(user_id=user.id).update(lastname=request.POST.get('lastname'))
            if 'email' in request.POST and request.POST.get('email') != '':
                Profile.objects.filter(user_id=user.id).update(email=request.POST.get('email'))
            data = {
                        'success':
                            {
                                'message':'New user successfuly created',
                            }
                    }
            return JsonResponse(data,status='200')
    else:
        return HttpResponse('',status='500')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def delUser(request):
    if 'id' in request.POST:
        user = User.objects.get(id=request.POST.get('id'))
        if request.user.id != user.id and user.id != 1:
            user.delete()
        else:
            error = {
                        'error':
                            {
                                'message':'Can\'t delete user. You might be trying to delete main admin or current logged on user.',
                            }
                    }
            return JsonResponse(error,status='200')
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted user '+user.username+'.',
                        }
                }
        return JsonResponse(data,status='200')
    elif 'id_list' in request.POST:
        deleted = 0
        notDeleted = 0
        for id in request.POST.get('id_list'):
            user = User.objects.get(id=id)
            if request.user.id != user.id and user.id != 1:
                user.delete()
                deleted +=1
            else:
                error['error'].append({'message':'Can\'t delete user. You might be trying to delete main admin or current logged on user.'})
                notDeleted += 1
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted '+deleted+' user'+('s' if deleted > 1 else '')+'.',
                        },
                    'error':
                        {
                            'message':'Failed to '+deleted+' user'+('s' if notDeleted > 1 else '')+'.',
                        }
                }
        return JsonResponse(data,status='200')
    else:
        return HttpResponse('',status='500')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def editUser(request):
    if 'id' in request.POST and request.POST.get('id') != '':
        id=request.POST.get('id')
        user = User.objects.filter(id=id);
        profile = Profile.objects.filter(user_id=id)
    else:
        data = {
                    'error':
                        {
                            'message':'Failed to edit user.',
                        }
                }
        return JsonResponse(data,status='200')
    if 'username' in request.POST and request.POST.get('username') != '':
        user.update(username=request.POST.get('username'))
    if 'password' in request.POST and request.POST.get('password') != '':
        userEntity = User.objects.get(id=id)
        userEntity.set_password(request.POST.get('password'))
        userEntity.save()
    if 'firstname' in request.POST and request.POST.get('firstname') != '':
        profile.update(firstname=request.POST.get('firstname'))
    if 'lastname' in request.POST and request.POST.get('lastname') != '':
        profile.update(lastname=request.POST.get('lastname'))
    if 'email' in request.POST and request.POST.get('email') != '':
        profile.update(email=request.POST.get('email'))
    data = {
                'success':
                    {
                        'message':'Successfuly edit user.',
                    }
            }
    return JsonResponse(data,status='200')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def changeUserStatus(request):
    if 'id' in request.POST and request.POST.get('id') != '':
        id= request.POST.get('id')
        user = User.objects.get(id=id)
        if request.user.id == user.id or user.id == 1:
            error = {
                        'error':
                            {
                                'message':'Can\'t change user status. You might be trying to change status of main admin or current logged on user.',
                            }
                    }
            return JsonResponse(error,status='200')
        status = request.POST.get('status')
        profile = Profile.objects.filter(user_id=id)
        profile.update(can_auth=False if Profile.objects.get(user_id=id).can_auth else True)
        data = {
                    'success':
                        {
                            'message':'Successfuly changed user status.',
                        }
                }
        return JsonResponse(data,status='200')
    else:
        data = {
                    'error':
                        {
                            'message':'Failed to change user status.',
                        }
                }
        return JsonResponse(data,status='200')
