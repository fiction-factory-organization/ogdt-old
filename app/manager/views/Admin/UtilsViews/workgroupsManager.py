# @Author: Thibaud FAURIE
# @Date:   2019-08-09T14:56:02+02:00
# @Project: OGDT
# @Filename: workgroupsManager.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-10T06:50:56+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.template import loader
from django.http import HttpResponse, JsonResponse

from manager.models import Workplace, Workgroup
from django.contrib.auth.models import User

import re

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

# WORKGROUPS MANAGER
@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def addWorkgroup(request):
    if 'label' in request.POST and 'workplace' in request.POST:
        # get label from POST
        label = request.POST.get('label')
        workplaceId = request.POST.get('workplace')
        # check if workplaces are not already using this label
        if Workgroup.objects.filter(label=label).exists():
            error = {
                        'error':
                            {
                                'message':'Workgroup label "'+label+'" already in use',
                            }
                    }
            return JsonResponse(error,status='200')
        else:
            if not Workplace.objects.filter(id=workplaceId).exists():
                error = {
                            'error':
                                {
                                    'message':"Specified workplace doesn't exist",
                                }
                        }
                return JsonResponse(error,status='200')
            workplace = Workplace.objects.get(id=workplaceId)
            workgroup = Workgroup.objects.create(label=label, workplace=workplace)
            workgroup.save()
            data = {
                        'success':
                            {
                                'message':'New workgroup successfuly created',
                            }
                    }
            return JsonResponse(data,status='200')
    else:
        error = {
                    'error':
                        {
                            'message':'Some properties are missing',
                        }
                }
        return JsonResponse(error,status='200')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def delWorkgroup(request):
    if 'id' in request.POST:
        workgroup = Workgroup.objects.get(id=request.POST.get('id'))
        workgroup.delete()
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted workplace '+workgroup.label+'.',
                        }
                }
        return JsonResponse(data,status='200')
    elif 'id_list' in request.POST:
        deleted = 0
        notDeleted = 0
        for id in request.POST.get('id_list'):
            workgroup = Workgroup.objects.get(id=id)
            workgroup.delete()
            deleted +=1
        data = {
                    'success':
                        {
                            'message':'Successfuly deleted '+deleted+' workgroup'+('s' if deleted > 1 else '')+'.',
                        },
                }
        return JsonResponse(data,status='200')
    else:
        error = {
                    'error':
                        {
                            'message':'No id provided',
                        }
                }
        return JsonResponse(error,status='200')

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def editWorkgroup(request):
    data = {'error':{'message':'','reasons':[]},'success':{'message':''}}
    error = False
    # Verify ID
    if 'id' in request.POST and request.POST.get('id') != '':
        id = request.POST.get('id')
        workgroup = Workgroup.objects.filter(id=id);
    else:
        data['error']['message'] = 'Failed to edit workgroup'
        data['error']['reasons'].append('Reason: No object id has been specified')
        return JsonResponse(data,status='200')
    # Verify Label
    if 'label' in request.POST and request.POST.get('label') != '':
        if Workgroup.objects.filter(label=request.POST.get('label')).exists():
            error = True
            data['error']['reasons'].append('Reason: Specified label is already in use')
        else:
            workgroup.update(label=request.POST.get('label'))
    # Verify workplace
    if 'workplace' in request.POST and request.POST.get('workplace') != '':
        if not Workplace.objects.filter(id=request.POST.get('workplace')).exists():
            error = True
            data['error']['reasons'].append("Reason: Specified workplace doesn't exist")
        else:
            workplace = Workplace.objects.get(id=request.POST.get('workplace'))
            workgroup.update(workplace=workplace)
    if error == True:
        data['error']['message'] = 'Failed to edit workplace'
    else:
        data['success']['message'] = 'Successfuly edit workplace'
    return JsonResponse(data,status='200')
