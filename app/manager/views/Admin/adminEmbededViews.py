# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:32+02:00
# @Project: OGDT
# @Filename: adminEmbededViews.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-09T15:46:50+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.template import loader
from django.http import HttpResponse

from manager.models import Profile, Cluster, Workgroup, Workplace, App, Project
from django.contrib.auth.models import User

from manager.common.decorators import ajax_required
from django.contrib.auth.decorators import login_required

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminGlobalSettings(request):
    context = {}
    template = loader.get_template('manager/Admin/GlobalSettings.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminSecurity(request):
    context = {}
    template = loader.get_template('manager/Admin/Security.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminUsers(request):
    queryset = Profile.objects.all()
    context = {'users':queryset}
    template = loader.get_template('manager/Admin/Users.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminPermissionsGroups(request):
    context = {}
    template = loader.get_template('manager/Admin/PermissionsGroups.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminWorkplaces(request):
    queryset = Workplace.objects.all()
    context = {'workplaces':queryset}
    template = loader.get_template('manager/Admin/Workplaces.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminClusters(request):
    queryset = Cluster.objects.all()
    context = {'clusters':queryset}
    template = loader.get_template('manager/Admin/Clusters.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminWorkgroups(request):
    queryset = Workgroup.objects.all()
    context = {'workgroups':queryset}
    template = loader.get_template('manager/Admin/Workgroups.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminProjects(request):
    queryset = Project.objects.all()
    context = {'projects':queryset}
    template = loader.get_template('manager/Admin/Projects.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminRessources(request):
    context = {}
    template = loader.get_template('manager/Admin/Ressources.html')
    return HttpResponse(template.render(context, request))

@ajax_required
@login_required(redirect_field_name="next",login_url='/login')
def adminApplications(request):
    queryset = App.objects.all()
    context = {'applications':queryset}
    template = loader.get_template('manager/Admin/Applications.html')
    return HttpResponse(template.render(context, request))
