# @Author: Thibaud FAURIE
# @Date:   2019-05-22T10:53:32+02:00
# @Project: OGDT Fusion
# @Filename: urls.py
# @Last modified by:   Thibaud FAURIE
# @Last modified time: 2019-08-10T06:46:51+02:00
# @License: GNU GPLv3
# @Copyright: Copyright (C) 2019 Thibaud FAURIE

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'profile', views.restViews.ProfileView, 'profile')
router.register(r'workplace', views.restViews.WorkplaceView, 'workplace')
router.register(r'workgroup', views.restViews.WorkgroupView, 'workgroup')
router.register(r'project', views.restViews.ProjectView, 'project')
router.register(r'user', views.restViews.UserView, 'user')

urlpatterns = [
    # Main views
    path('', views.index),
    path('workspace', views.views.workspace),
    path('login', views.views.login),
    path('logout', views.views.logout),
    path('session', views.views.session),

    path('api/', include(router.urls)),

    # Workplaces Pannel
    path('ajax/wp', views.embededViews.workplaceSelector),

    # Profile pannel
    path('ajax/profile', views.embededViews.profilePanel),
    path('ajax/profile/details', views.profileEmbededViews.profileDetails),
    path('ajax/profile/security', views.profileEmbededViews.profileSecurity),
    path('ajax/profile/preferences', views.profileEmbededViews.profilePreferences),

    path('ajax/profile/username', views.profileUtilsViews.changeUsername),
    path('ajax/profile/password', views.profileUtilsViews.changePassword),

    # Admin pannel
    path('ajax/admin', views.embededViews.adminPanel),
    path('ajax/admin/global_settings', views.adminEmbededViews.adminGlobalSettings),
    path('ajax/admin/security', views.adminEmbededViews.adminSecurity),
    path('ajax/admin/users', views.adminEmbededViews.adminUsers),
    path('ajax/admin/permissions_groups', views.adminEmbededViews.adminPermissionsGroups),
    path('ajax/admin/workplaces', views.adminEmbededViews.adminWorkplaces),
    path('ajax/admin/clusters', views.adminEmbededViews.adminClusters),
    path('ajax/admin/workgroups', views.adminEmbededViews.adminWorkgroups),
    path('ajax/admin/projects', views.adminEmbededViews.adminProjects),
    path('ajax/admin/ressources', views.adminEmbededViews.adminRessources),
    path('ajax/admin/applications', views.adminEmbededViews.adminApplications),

    # Users CRUD
    path('ajax/admin/users/add', views.adminUtilsViews.usersManager.addUser),
    path('ajax/admin/users/del', views.adminUtilsViews.usersManager.delUser),
    path('ajax/admin/users/edit', views.adminUtilsViews.usersManager.editUser),
    path('ajax/admin/users/status', views.adminUtilsViews.usersManager.changeUserStatus),

    # Workplaces CRUD
    path('ajax/admin/workplaces/add', views.adminUtilsViews.workplacesManager.addWorkplace),
    path('ajax/admin/workplaces/del', views.adminUtilsViews.workplacesManager.delWorkplace),
    path('ajax/admin/workplaces/edit', views.adminUtilsViews.workplacesManager.editWorkplace),

    # Workgroups CRUD
    path('ajax/admin/workgroups/add', views.adminUtilsViews.workgroupsManager.addWorkgroup),
    path('ajax/admin/workgroups/del', views.adminUtilsViews.workgroupsManager.delWorkgroup),
    path('ajax/admin/workgroups/edit', views.adminUtilsViews.workgroupsManager.editWorkgroup),

    # App pannel
    path('ajax/apps', views.embededViews.appSelector),

    # Workgroup, Project utils
    path('ajax/wg', views.utilsViews.workgroupRegister),
    path('ajax/wg/del', views.utilsViews.workgroupUnregister),
    path('ajax/pj', views.utilsViews.projectRegister),
    path('ajax/pj/del', views.projectUnregister),

    # Tab utils
    path('ajax/tabs/save', views.utilsViews.saveTab),
    path('ajax/tabs/del', views.utilsViews.delTab),
    path('ajax/tabs/load', views.utilsViews.loadTabs),

    # Global utils
    path('ajax/objects/check', views.utilsViews.checkObject),
]
