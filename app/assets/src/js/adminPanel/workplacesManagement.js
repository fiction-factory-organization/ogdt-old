/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-07-30T02:00:46+02:00
 * @Project: OGDT
 * @Filename: workplacesManagement.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T03:12:16+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

var workplacesDataTable = null;

$(document).ready(function(){

  var timeout_label = null;
  var timeout_urlPrefix = null;

  $(".workplacesManagement_label").on('input', function(e){

    var that = this;
    var modal = $(that).closest('.modal');
    $(modal).find('.workplacesManagement_submit').prop('disabled', true);

    let error = false;
    let data = {};

    $(that).removeClass('is-invalid is-valid');
    $(modal).find('.workplacesManagement_label_feedback').removeClass('valid-feedback invalid-feedback');
    $(modal).find('.workplacesManagement_label_append_placeholder').hide();
    $(modal).find('.workplacesManagement_label_feedback').html('');
    $(modal).find('.workplacesManagement_label_spinner').show();

    if (timeout_label !== null) {
        clearTimeout(timeout_label);
    }

    if($(this).val() !== ''){
      url = CONST.URLS.EXIST_URL;
      data = {'workplace.label':$(this).val()};
    }else{
      if($(modal).attr("id") == "workplacesDataTableEdit"){
        $(modal).find('.workplacesManagement_submit').prop('disabled', false);
      }
      error = true;
    }

    timeout_label = setTimeout(function () {
      if(error != true){
        $.ajax({
              type     : "POST",
              url      : url,
              data     : data,
              success  : function(data)
              {
                $(modal).find('.workplacesManagement_label_spinner').hide();
                $(modal).find('.workplacesManagement_label_append_placeholder').show();
                if (data['error']) {
                  $(that).addClass('is-invalid');
                  $(modal).find('.workplacesManagement_label_feedback').addClass('invalid-feedback');
                  $(modal).find('.workplacesManagement_label_feedback').html(data['error']['message']);
                  $(modal).find('.workplacesManagement_submit').prop('disabled', true);
                }else{
                  $(that).addClass('is-valid');
                  $(modal).find('.workplacesManagement_label_feedback').addClass('valid-feedback');
                  $(modal).find('.workplacesManagement_label_feedback').html('Label is available');
                  $(modal).find('.workplacesManagement_submit').prop('disabled', false);
                }
              },
              error: function(data){

              },
        });
      }else{
        $(modal).find('.workplacesManagement_label_spinner').hide();
        $(modal).find('.workplacesManagement_label_append_placeholder').show();
      }
    }, 1000);
  });

  $(".workplacesManagement_urlPrefix").on('input', function(e){

    var that = this;
    var modal = $(that).closest('.modal');
    $(modal).find('.workplacesManagement_submit').prop('disabled', true);

    let error = false;
    let data = {};

    $(that).removeClass('is-invalid is-valid');
    $(modal).find('.workplacesManagement_urlPrefix_feedback').removeClass('valid-feedback invalid-feedback');
    $(modal).find('.workplacesManagement_urlPrefix_append_placeholder').hide();
    $(modal).find('.workplacesManagement_urlPrefix_feedback').html('');
    $(modal).find('.workplacesManagement_urlPrefix_spinner').show();

    if (timeout_urlPrefix !== null) {
        clearTimeout(timeout_urlPrefix);
    }

    if($(this).val() !== ''){
      url = CONST.URLS.EXIST_URL;
      data = {'workplace.urlPrefix':$(this).val()};
    }else{
      if($(modal).attr("id") == "workplacesDataTableEdit"){
        $(modal).find('.workplacesManagement_submit').prop('disabled', false);
      }
      error = true;
    }

    timeout_urlPrefix = setTimeout(function () {
      if(error != true){
        $.ajax({
              type     : "POST",
              url      : url,
              data     : data,
              success  : function(data)
              {
                $(modal).find('.workplacesManagement_urlPrefix_spinner').hide();
                $(modal).find('.workplacesManagement_urlPrefix_append_placeholder').show();
                if (data['error']) {
                  $(that).addClass('is-invalid');
                  $(modal).find('.workplacesManagement_urlPrefix_feedback').addClass('invalid-feedback');
                  $(modal).find('.workplacesManagement_urlPrefix_feedback').html(data['error']['message']);
                  $(modal).find('.workplacesManagement_submit').prop('disabled', true);
                }else{
                  $(that).addClass('is-valid');
                  $(modal).find('.workplacesManagement_urlPrefix_feedback').addClass('valid-feedback');
                  $(modal).find('.workplacesManagement_urlPrefix_feedback').html('Url Prefix is available');
                  $(modal).find('.workplacesManagement_submit').prop('disabled', false);
                }
              },
              error: function(data){

              },
        });
      }else{
        $(modal).find('.workplacesManagement_urlPrefix_spinner').hide();
        $(modal).find('.workplacesManagement_urlPrefix_append_placeholder').show();
      }
    }, 1000);
  });

  $('#workplacesDataTableAction').selectpicker();
  $('[data-toggle="tooltip"]').tooltip();
});

// function randomString(element,targetId,parentId){
//   string =  Math.random().toString(36).substring(2, 15);
//   $('#'+parentId).find('.'+targetId).val(string);
// }

function workplacesManagement(element, action, id = null)
{
  let error = false;
  let data = {};

  $(element).find('span.button_text').find('span.button_icon').hide();
  $(element).attr('disabled', true);
  $(element).find('span.spinner-grow').show();

  switch(action) {
    case 'add':
      modal = $('#workplacesDataTableAdd');
      elementId = '#';
      url = CONST.URLS.UTILS.WORKPLACE.ADD;
      if ($(modal).find('.workplacesManagement_label').val() !== '') {
        data['label'] = $(modal).find('.workplacesManagement_label').val();
      }else{
        addMessage('Label can\'t be empty', 'warning');
        error = true;
      }

      if ($(modal).find('.workplacesManagement_urlPrefix').val() !== '') {
        data['url_prefix'] = $(modal).find('.workplacesManagement_urlPrefix').val();
      }else{
        addMessage('Url Prefix can\'t be empty', 'warning');
        error = true;
      }

      ownertype = $(modal).find('.workplacesManagement_ownerType');
      if ($(ownertype).find(":selected").val() !== '') {
        data['owner_type'] = $(ownertype).find(":selected").val();
      }else{
        addMessage('Owner Type can\'t be empty', 'warning');
        error = true;
      }

      timezone = $(modal).find('.workplacesManagement_timeZone');
      if ($(timezone).find(":selected").val() !== '') {
        data['time_zone'] = $(timezone).find(":selected").val();
      }else{
        addMessage('Time Zone can\'t be empty', 'warning');
        error = true;
      }

      if (error == true) {
        $(element).find('span.button_text').find('span.button_icon').show();
        $(element).attr('disabled', false);
        $(element).find('span.spinner-grow').hide();
        break;
      }

      console.log(data);

      $.ajax({
            type     : "POST",
            url      : url,
            data     : data,
            success  : function(data)
            {
                if (data['error']) {
                  addMessage(data['error']['message'], 'danger')
                  $(element).attr('disabled', false);
                  $(element).find('span.button_text').find('span.button_icon').show();
                  $(element).find('span.spinner-grow').hide();
                }else{
                  addMessage('New workplace successfuly created', 'success');
                  $(element).find('span.button_text').find('span.button_icon').show();
                  $(element).attr('disabled', false);
                  $(element).find('span.spinner-grow').hide();
                  $(element).closest('.modal').modal('hide');
                  refreshTab('#admin_workplaces','/ajax/admin/workplaces');
                }
            },
            error: function()
            {
                addMessage('Failed to create new workplace','warning');
                $(element).attr('disabled', false);
                $(element).find('span.button_text').find('span.button_icon').show();
                $(element).find('span.spinner-grow').hide();
            },
      });
      break;

    case 'edit':
      data = {'id':id};
      url = CONST.URLS.UTILS.WORKPLACE.EDIT;
      modal = $('#workplacesDataTableEdit');

      data['label'] = $(modal).find('.workplacesManagement_label').val() || '';
      data['url_prefix'] = $(modal).find('.workplacesManagement_urlPrefix').val() || '';
      data['owner_type'] = $(modal).find('.workplacesManagement_ownerType').find(":selected").val() || '';
      data['time_zone'] = $(modal).find('.workplacesManagement_timeZone').find(":selected").val() || '';

      $.ajax({
            type     : "POST",
            url      : url,
            data     : data,
            success  : function(data)
            {
                if (data['error']['message'] != '') {
                  var message = data['error']['message'];
                  $.each(data['error']['reasons'], function(index, reason) {
                    message += '<br/>'+reason;
                  });
                  addMessage(message,'danger');
                } else if (data['success']['message'] != '') {
                  addMessage(data['success']['message'], 'success');
                  refreshTab('#admin_workplaces','/ajax/admin/workplaces');
                }
                $(element).attr('disabled', false);
                $(element).find('span.button_text').find('span.button_icon').show();
                $(element).find('span.spinner-grow').hide();
                $(element).closest('.modal').modal('hide');
            },
            error: function()
            {
              addMessage('Failed to edit workplace (Time out)','warning');
              $(element).attr('disabled', false);
              $(element).find('span.button_text').find('span.button_icon').show();
              $(element).find('span.spinner-grow').hide();
            },
      });
      break;

    // case 'deleteM':
    //   var idList = $.map($('#userDataTable').DataTable().table(0).rows('.selected').data(), function (item) {
    //       return item[1];
    //   });
    //   alert(idList);
    //   break;

    case 'delete':
      modal = $('#workplacesDataTableDelete');
      data = {'id':id};
      url = CONST.URLS.UTILS.WORKPLACE.DEL;
      $.ajax({
            type     : "POST",
            url      : url,
            data     : data,
            success  : function(data)
            {
                let message = ''
                if (data['error']) {
                  message += 'Failed to delete workplace '+$('#workplace_'+id).text()+':<br/>'+data['error']['message'];
                  addMessage(message,'danger');

                } else if (data['success']) {
                  addMessage(data['success']['message'], 'success');
                  refreshTab('#admin_workplaces','/ajax/admin/workplaces');
                }
            },
            error: function()
            {
                addMessage('Failed to delete workplace (Time out)','warning');
                $(element).attr('disabled', false);
                $(element).find('span.button_text').find('span.button_icon').show();
                $(element).find('span.spinner-grow').hide();
            },
      });
      break;
  }
  $(modal).modal('hide');
}
