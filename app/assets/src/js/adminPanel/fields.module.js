/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-14T01:11:35+02:00
 * @Project: OGDT
 * @Filename: field.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-14T01:13:51+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

 $(document).ready(function(){

   var timeout_username = null;
   var timeout_email = null;

   $(".usersManagement_username").on('input', function(e){

     var that = this;
     var modal = $(that).closest('.modal');
     $(modal).find('.usersManagement_submit').prop('disabled', true);

     let error = false;
     let data = {};

     $(that).removeClass('is-invalid is-valid');
     $(modal).find('.usersManagement_username_feedback').removeClass('valid-feedback invalid-feedback');
     $(modal).find('.usersManagement_username_append_placeholder').hide();
     $(modal).find('.usersManagement_username_feedback').html('');
     $(modal).find('.usersManagement_username_spinner').show();

     if (timeout_username !== null) {
         clearTimeout(timeout_username);
     }

     if($(this).val() !== ''){
       url = CONST.URLS.EXIST_URL;
       data = {'user.username':$(this).val()};
     }else{
       if($(modal).attr("id") == "userDataTableEdit"){
         $(modal).find('.usersManagement_submit').prop('disabled', false);
       }
       error = true;
     }

     timeout_username = setTimeout(function () {
       if(error != true){
         $.ajax({
               type     : "POST",
               url      : url,
               data     : data,
               success  : function(data)
               {
                 $(modal).find('.usersManagement_username_spinner').hide();
                 $(modal).find('.usersManagement_username_append_placeholder').show();
                 if (data['error']) {
                   $(that).addClass('is-invalid');
                   $(modal).find('.usersManagement_username_feedback').addClass('invalid-feedback');
                   $(modal).find('.usersManagement_username_feedback').html(data['error']['message']);
                   $(modal).find('.usersManagement_submit').prop('disabled', true);
                 }else{
                   $(that).addClass('is-valid');
                   $(modal).find('.usersManagement_username_feedback').addClass('valid-feedback');
                   $(modal).find('.usersManagement_username_feedback').html('Username is available');
                   $(modal).find('.usersManagement_submit').prop('disabled', false);
                 }
               },
               error: function(data){

               },
         });
       }else{
         $(modal).find('.usersManagement_username_spinner').hide();
         $(modal).find('.usersManagement_username_append_placeholder').show();
       }
     }, 1000);
   });

   $(".usersManagement_mail").on('input', function(e){

     var that = this;
     var modal = $(that).closest('.modal');
     $(modal).find('.usersManagement_submit').prop('disabled', true);

     let error = false;
     let data = {};

     $(that).removeClass('is-invalid is-valid');
     $(modal).find('.usersManagement_mail_feedback').removeClass('valid-feedback invalid-feedback');
     $(modal).find('.usersManagement_mail_append_placeholder').hide();
     $(modal).find('.usersManagement_mail_feedback').html('');
     $(modal).find('.usersManagement_mail_spinner').show();

     if (timeout_email !== null) {
         clearTimeout(timeout_email);
     }

     if($(this).val() !== ''){
       url = CONST.URLS.EXIST_URL;
       data = {'profile.email':$(this).val()};
     }else{
       if($(modal).attr("id") == "userDataTableEdit"){
         $(modal).find('.usersManagement_submit').prop('disabled', false);
       }
       error = true;
     }

     timeout_email = setTimeout(function () {
       if (!CONST.EMAIL_REGEX.test($(that).val()) && error !== true) {
         error = true;
         $(that).addClass('is-invalid');
         $(modal).find('.usersManagement_mail_feedback').addClass('invalid-feedback');
         $(modal).find('.usersManagement_mail_feedback').html('Please provide a valid email');
         $(modal).find('.usersManagement_submit').prop('disabled', true);
       }
       if(error != true){
         $.ajax({
               type     : "POST",
               url      : url,
               data     : data,
               success  : function(data)
               {
                 $(modal).find('.usersManagement_mail_spinner').hide();
                 $(modal).find('.usersManagement_mail_append_placeholder').show();
                 if (data['error']) {
                   $(that).addClass('is-invalid');
                   $(modal).find('.usersManagement_mail_feedback').addClass('invalid-feedback');
                   $(modal).find('.usersManagement_mail_feedback').html(data['error']['message']);
                   $(modal).find('.usersManagement_submit').prop('disabled', true);
                 }else{
                   $(that).addClass('is-valid');
                   $(modal).find('.usersManagement_mail_feedback').addClass('valid-feedback');
                   $(modal).find('.usersManagement_mail_feedback').html('Email address is available');
                   $(modal).find('.usersManagement_submit').prop('disabled', false);
                 }
               },
               error: function(data){

               },
         });
       }else{
         $(modal).find('.usersManagement_mail_spinner').hide();
         $(modal).find('.usersManagement_mail_append_placeholder').show();
       }
     }, 1000);
   });

   $('#userDataTableAction').selectpicker();
   $('[data-toggle="tooltip"]').tooltip();
 });
