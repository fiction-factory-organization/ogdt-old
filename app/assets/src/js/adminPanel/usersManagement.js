/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-05-26T02:30:57+02:00
 * @Project: OGDT
 * @Filename: usersManagement.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-14T01:21:25+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

var userDataTable = null;

function usersManagement(element, action, id = null)
{
  let error = false;
  let data = {};

  switch(action) {
    case 'add':
      modal = $('#userDataTableAdd');
      button = element;
      data['username'] = $(modal).find('.usersManagement_username').val() || '';
      data['password'] = $(modal).find('.usersManagement_password').val() || '';
      data['email'] = $(modal).find('.usersManagement_mail').val() || '';
      data['firstname'] = $(modal).find('.usersManagement_firstname').val() || '';
      data['lastname'] = $(modal).find('.usersManagement_lastname').val() || '';
      verifyAndAddUser(data, button);
      break;

    case 'edit':
      modal = $('#userDataTableEdit');
      button = element;
      data = {'id':id};
      data['username'] = $(modal).find('.usersManagement_username').val() || '';
      data['password'] = $(modal).find('.usersManagement_password').val() || '';
      data['email'] = $(modal).find('.usersManagement_mail').val() || '';
      data['firstname'] = $(modal).find('.usersManagement_firstname').val() || '';
      data['lastname'] = $(modal).find('.usersManagement_lastname').val() || '';
      verifyAndEditUser(data, button);
      break;

    // case 'deleteM':
    //   var idList = $.map($('#userDataTable').DataTable().table(0).rows('.selected').data(), function (item) {
    //       return item[1];
    //   });
    //   alert(idList);
    //   break;

    case 'delete':
      modal = $('#userDataTableDelete');
      button = element;
      data = {'id':id};
      verifyAndDeleteUser(data, button);
      break;
  //
  //   case 'enableM':
  //     var idList = $.map($('#userDataTable').DataTable().table(0).rows('.selected').data(), function (item) {
  //         return item[1];
  //     });
  //     alert(idList);
  //     break;
  //
    case 'enable':
      modal = $('#userDataTableEnable');
      data = {'id':id};
      verifyAndEnableUser(data, null);
      break;
  //
  //   case 'disableM':
  //     var idList = $.map($('#userDataTable').DataTable().table(0).rows('.selected').data(), function (item) {
  //         return item[1];
  //     });
  //     alert(idList);
  //     break;
  //
    case 'disable':
      modal = $('#userDataTableDisable');
      data = {'id':id};
      verifyAndDisableUser(data, null);
      break;
  }
  //
  $(modal).modal('hide');
}

function verifyAndAddUser (data, button) {

  if (data['username'] == '') {
    addMessage('Username can\'t be empty', 'warning');
    return;
  }

  if (data['password'] == '') {
    addMessage('Password can\'t be empty', 'warning');
    return;
  }

  if (data['email'] == '') {
    addMessage('Email can\'t be empty', 'warning');
    return;
  }

  $.ajax({
        type     : "POST",
        url      : CONST.URLS.UTILS.USER.ADD,
        data     : data,
        success  : function(data)
        {
          if (data['error']) {
            addMessage('Failed to create new user','warning');
            addMessage(data['error']['message'], 'danger')
          } else if (data['success']['message'] != '') {
            addMessage(data['success']['message'], 'success');
            refreshTab('#admin_users','/ajax/admin/users');
          } else {
            addMessage('Unkown state', 'warning');
          }
          setButtonActive(button);
        },
        error: function()
        {
          setButtonActive(button);
          addMessage('Failed to create new user (Unkown error)','danger');
        },
  });
}

function verifyAndEditUser (data, button) {

  $.ajax({
        type     : "POST",
        url      : CONST.URLS.UTILS.USER.EDIT,
        data     : data,
        success  : function(data)
        {
          if (data['error']) {
            addMessage('Failed to edit user','warning');
            addMessage(data['error']['message'],'danger');
          } else if (data['success']) {
            addMessage(data['success']['message'], 'success');
            refreshTab('#admin_users','/ajax/admin/users');
          } else {
            addMessage('Unkown state', 'warning');
          }
          setButtonActive(button);
        },
        error: function()
        {
          setButtonActive(button);
          addMessage('Failed to edit user (Unkown error)','danger');
        },
  });
}

function verifyAndDeleteUser (data, button) {

  id = data['id'];

  $.ajax({
        type     : "POST",
        url      : CONST.URLS.UTILS.USER.DEL,
        data     : data,
        success  : function(data)
        {
          let message = ''
          if (data['error']) {
            message += 'Failed to delete user '+$('#username_'+id).text()+':<br/>'+data['error']['message'];
            addMessage(message,'danger');
          } else if (data['success']) {
            addMessage(data['success']['message'], 'success');
            refreshTab('#admin_users','/ajax/admin/users');
          } else {
            addMessage('Unkown state', 'warning');
          }
          setButtonActive(button);
        },
        error: function()
        {
          setButtonActive(button);
          addMessage('Failed to delete user (Unkown error)','danger');
        },
  });
}

function verifyAndEnableUser (data, button) {

  id = data['id'];

  $.ajax({
        type     : "POST",
        url      : CONST.URLS.UTILS.USER.CHANGE_STATUS,
        data     : data,
        success  : function(data)
        {
          let message = ''
          if (data['error']) {
            message += 'Failed to enable user '+$('#username_'+id).text()+':<br/>'+data['error']['message'];
            addMessage(message,'danger');

          } else if (data['success']) {
            addMessage(data['success']['message'], 'success');
            refreshTab('#admin_users','/ajax/admin/users');
          } else {
            addMessage('Unkown state', 'warning');
          }
        },
        error: function()
        {
          setButtonActive(button);
          addMessage('Failed to enable user (Unkown error)','danger');
        },
  });
}

function verifyAndDisableUser (data, button) {

  id = data['id'];

  $.ajax({
        type     : "POST",
        url      : CONST.URLS.UTILS.USER.CHANGE_STATUS,
        data     : data,
        success  : function(data)
        {
          let message = ''
          if (data['error']) {
            message += 'Failed to disable user '+$('#username_'+id).text()+':<br/>'+data['error']['message'];
            addMessage(message,'danger');
          } else if (data['success']) {
            addMessage(data['success']['message'], 'success');
            refreshTab('#admin_users','/ajax/admin/users');
          } else {
            addMessage('Unkown state', 'warning');
          }
        },
        error: function()
        {
          setButtonActive(button);
          addMessage('Failed to disable user (Unkown error)','danger');
        },
  });
}
