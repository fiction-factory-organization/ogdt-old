/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-10T02:33:39+02:00
 * @Project: OGDT
 * @Filename: form.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-14T01:12:37+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

function resetFormValidation(form){
 $(form).find('.is-valid').each(function(){
   $(this).removeClass('is-valid');
 });
 $(form).find('.is-invalid').each(function(){
   $(this).removeClass('is-invalid');
 });
 let modal = $(form).closest('.modal');
 switch($(modal).attr('id')){
   case "userDataTableEdit":
     $(modal).find('.submit_button').prop('disabled', false);
     break;
   case "workplacesDataTableEdit":
     $(modal).find('.submit_button').prop('disabled', false);
     break;
   default:
     $(modal).find('.submit_button').prop('disabled', true);
     break;
 }
}

function randomString(element,targetId,parentId){
  string =  Math.random().toString(36).substring(2, 15);
  $('#'+parentId).find('.'+targetId).val(string);
}
