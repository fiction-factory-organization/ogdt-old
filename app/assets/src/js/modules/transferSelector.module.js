/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-09-05T22:53:02+02:00
 * @Project: OGDT
 * @Filename: transferSelector.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-09-06T00:46:44+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

TRANSFER = {
  TABLE: {
   AVAILABLE: "transfer_available",
   SELECTED: "transfer_selected",
  },
}

Transfer = class {

  constructor(element){
    this.element = element;
    console.log($(element).find('.button-right')[0]);
    $(element).find('.button-right').on('click',()=>{
      this.transferRight();
    });
    $(element).find('.button-left').on('click',()=>{
      this.transferLeft();
    });
  }

  add(table,data) {
    var newItem = `<div class="transfer_item"><label class="checkmark_container" for="${data.id}">${data.label}<input type="checkbox" id="${data.id}"><span class="checkmark"></span></label></div>`;
    $(this.element).find('.'+table).find('.list-body').append(newItem);
    $(this.element).find('#'+data.id).on('click',function(){
      var checkbox = $(this);
      if($(checkbox)[0].hasAttribute("checked")){
        $(checkbox.attr('checked',false));
        console.log('Unchecked');
      }else{
        $(checkbox.attr('checked',true));
        console.log('Checked');
      }
    });
  }

  remove(table,data) {
    $(this.element).find('.'+table).find('.list-body').find('#'+data.id).parent().parent().remove();
  }

  transferRight() {
    var items = this.getCheckedFromLeft();
    items.forEach((item)=>{
      this.remove(TRANSFER.TABLE.AVAILABLE,item);
      this.add(TRANSFER.TABLE.SELECTED,{id:item.id,label:item.label});
    });
  }

  transferLeft() {
    var items = this.getCheckedFromRight();
    items.forEach((item)=>{
      this.remove(TRANSFER.TABLE.SELECTED,item);
      this.add(TRANSFER.TABLE.AVAILABLE,{id:item.id,label:item.label});
    });
  }

  getSelected() {
    var elements = $(this.element).find('.'+TRANSFER.TABLE.SELECTED).find('.transfer_item');
    var items = [];
    elements.each(function(element){
      items.push({id:$(element).find('label input').attr('id'),label:$(element).find('label').text()});
    });
    return items;
  }

  getAvailable() {
    var elements = $(this.element).find('.'+TRANSFER.TABLE.AVAILABLE).find('.transfer_item');
    var items = [];
    elements.each(function(element){
      items.push({id:$(element).find('label input').attr('id'),label:$(element).find('label').text()});
    });
    return items;
  }

  getAll() {
    var elements = $(this.element).find('.transfer_item');
    var items = [];
    elements.each(function(element){
      items.push({id:$(element).find('label input').attr('id'),label:$(element).find('label').text()});
    });
    return items;
  }

  getCheckedFromLeft() {
    var elements = $(this.element).find('.'+TRANSFER.TABLE.AVAILABLE+' input:checked');
    var items = [];
    elements.each(function(element){
      items.push({id:$(elements[element]).attr('id'),label:$(elements[element]).parent().text()});
    });
    return items;
  }

  getCheckedFromRight() {
    var elements = $(this.element).find('.'+TRANSFER.TABLE.SELECTED+' input:checked');
    var items = [];
    elements.each(function(element){
      items.push({id:$(elements[element]).attr('id'),label:$(elements[element]).parent().text()});
    });
    return items;
  }
}
