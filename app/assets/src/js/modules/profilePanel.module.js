//
// OpenGDToolbox : collaborative web app made for game creators to simplify game
// design work flow and collaboration.
// Copyright (C) 2019 Thibaud FAURIE
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
//

const CHANGE_USERNAME_URL = '/ajax/profile/username';
const CHANGE_PASSWORD_URL = '/ajax/profile/password';

$("#profile-tab a").on('show.bs.tab', function (e) {
  var tab = $(this).attr('href');
  var url = '';

  switch(tab) {
    case "#profile_details":
      url = '/ajax/profile/details';
      break;
    case "#profile_security":
      url = '/ajax/profile/security';
      break;
    case "#profile_prefs":
      url = '/ajax/profile/preferences';
      break;
  }

  if(url != ''){
    refreshTab(tab, url);
  }

});

function profileSecurity(element, action, item){

  let error = false;
  let data = {};

  $(element).find('span.button_text').find('i').hide();
  $(element).attr('disabled', true);
  $(element).find('span.spinner-grow').show();
  switch(action) {
    case 'submit':
      switch(item) {
        case 'username':
          if ($('#security_username').val() !== '') {
            url = CHANGE_USERNAME_URL;
            data = {'username':$('#security_username').val()}
          }else{
            addMessage('Username can\'t be empty', 'warning');
            error = true;
          }
          break;
        case 'password':
          url = CHANGE_PASSWORD_URL;
          if ($('#security_password').val() == '') {
            addMessage('Password can\'t be empty', 'warning');
            error = true;
          }
          if ($('#security_password').val() == $('#security_password-repeat').val()) {
            data = {'password':$('#security_password').val()}
          }else{
            addMessage('Passwords must match', 'warning');
            error = true;
          }
          break;
      }

      if (error == true) {
        $(element).find('span.button_text').find('i').show();
        $(element).attr('disabled', false);
        $(element).find('span.spinner-grow').hide();
        break;
      }

      $.ajax({
            type     : "POST",
            url      : url,
            data     : data,
            success  : function(data)
            {
                addMessage('New '+item+' successfuly set', 'success');
                $(element).find('span.button_text').find('i').show();
                $(element).attr('disabled', false);
                refreshTab('#profile_security','/ajax/profile/security');
                $(element).find('span.spinner-grow').hide();
            },
            error: function(){
                addMessage('Failed to set '+item, 'danger');
                if (data['error']) {
                  addMessage(data['error']['message'], 'danger')
                }
                if (data['warning']) {
                  addMessage(data['warning']['message'], 'danger')
                }
                $(element).find('span.button_text').find('i').show();
                $(element).find('span.button_text').find('i').addClass('fa-time');
                $(element).find('span.button_text').find('i').removeClass('fa-check');
                $(element).find('span.spinner-grow').hide();
                $(element).addClass('btn-danger');
                $(element).removeClass('btn-primary');
            },
      });
      break;
  }
}
