/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-05-22T10:53:31+02:00
 * @Project: OGDT
 * @Filename: workplaceSelector.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-09-05T16:15:12+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


function registerWorkplace(id, button, cb){
  $('#loading-spinner').show();

  $(button).prop('disabled', true);
  $(button).html(
    `Loading...`
  );

  $.ajax({
        type     : "POST",
        url      : '/ajax/wp',
        data     : {workplace: id},
        cache    : false,
        success  : function()
        {
            unregisterWorkgroup(true);
            addMessage('Successfuly registered to workplace.', 'success');
            $('#workplaces_list .card').each(function(){
              $(this).find('.card-header').removeClass('bg-success');
              $(this).removeClass('border-success');
              $(this).find('.card-body button').removeClass('btn-success');
              $(this).find('.card-body button').addClass('btn-secondary');
              $(this).find('.card-body button').html('<i class="fas fa-check"></i>');
              $(this).find('.card-body button').prop('disabled', false);
            });

            $(button).parent().prev().addClass('bg-success');
            $(button).parent().parent().addClass('border-success');
            $(button).removeClass('btn-secondary');
            $(button).addClass('btn-success');
            $(button).html(
              `Registered`
            );
            $(button).prop('disabled', true);
            $('#loading-spinner').hide();

            $('#workgroup_dropdown').trigger('content:reload');
            $('#project_dropdown').trigger('content:reload');
            var reloadCallback = function(element){
              var tab = $('a[href^="'+'#'+$(element).attr('id')+'"]');
              var classListString = $(tab).attr('class');
              var regex = /(app-)\w+/g;
              var matchingString = classListString.match(regex);
              var appName = matchingString[0].split('-')[1];
              refreshTab('#'+$(element).attr('id'),CONST.TABREFS.APPS[appName].url);
            }
            // reload active pane
            $('#dashboard-tab-content > .tab-pane.active').trigger('content:reload',reloadCallback);
            // reload workplace selector by default even when not active
            $('#dashboard-tab-content > '+$('.app-workplaces').attr('href')).trigger('content:reload',reloadCallback);
        },
        error: function(){

            addMessage('Failed to register in workplace.', 'danger');
            $(button).removeClass('btn-secondary');
            $(button).addClass('btn-alert');
            $(button).html(
              `Fail !`
            );
            $('#loading-spinner').hide();
        },
  });
}
