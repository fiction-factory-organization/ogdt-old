/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-14T00:43:41+02:00
 * @Project: OGDT
 * @Filename: buttons.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-14T00:45:07+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

function setButtonActive (button) {
  $(button).find('span.button_text').find('span.button_icon').show();
  $(button).attr('disabled', false);
  $(button).find('span.spinner-grow').hide();
}

function setButtonProcessing (button) {
  $(button).find('span.button_text').find('span.button_icon').hide();
  $(button).attr('disabled', true);
  $(button).find('span.spinner-grow').show();
}
