/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-05-22T10:53:31+02:00
 * @Project: OGDT
 * @Filename: tab.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-09-05T16:48:09+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

var currentConfirmObject;

var currentTab = $('#addTab');
var tabCount = 0;
var usedIds = [];
const menu = document.querySelector("#tab-context-menu");
let menuVisible = false;
//initilize tabs

const toggleMenu = command => {
  menu.style.display = command === "show" ? "block" : "none";
};

const setPosition = ({ top, left }) => {
  menu.style.left = `${left}px`;
  menu.style.top = `${top}px`;
  menuVisible = true;
  toggleMenu('show');
};

$(function () {

    //horizontal scroll
    $("#dashboard-tab").mousewheel(function(event, delta) {

       this.scrollLeft -= (delta * 30);
       event.preventDefault();

    });

    //when ever any tab is clicked this method will be call
    $("#dashboard-tab").on("click", "a", function (e) {
        e.preventDefault();

        if($(this).attr('id')!="addTab"){
          $(this).tab('show');
          currentTab = $(this);
        }
    });

    $("#dashboard-tab").on("contextmenu", "li", function (e) {
        e.preventDefault();
        const origin = {
            left: e.pageX,
            top: e.pageY
          };
        currentConfirmObject = $(this);
        setPosition(origin);
        return false;
    });

    $("html").on("click", function(e){

      if(menuVisible){
        toggleMenu("hide");
      }
    });

    registerAddTabButtonEvent();
    registerCloseEvent();
});

//add tab dynamically and register click event
function registerAddTabButtonEvent() {

    $('#addTab').click(function (e) {
        e.preventDefault();

        addTabWithCheck('New tab','/ajax/apps','apps');

    });

}

//register event on close icon on the tab
function registerCloseEvent() {

    $(".closeTab").off("click");

    $(".closeTab").on("click", function () {

        //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
        var tab = this
        bootbox.confirm({size: "small",message: "Are you sure?",callback: function(result){ if(result){removeTab(tab);}}, className:"square-modal"});

    });
}

function removeTab(object) {
  var tabContentId = $(object).parent().attr("href");
  var tabId = $(object).parent().attr("id");
  var tabToShow;
  var tabPos;

  if($(object).parent().attr("href")==$( "#dashboard-tab li:nth-last-child(2) a" ).attr("href")){
    tabToShow = $( "#dashboard-tab li:nth-last-child(3) a" );
  }else if($(object).parent().hasClass("active")){
    tabPos = $(object).parent().parent().index()+2;
    tabToShow = $( "#dashboard-tab li:nth-child(" + tabPos + ") a" );
  }

  delTab(tabId);

  $(object).parent().parent().remove(); //remove li of tab
  $(tabContentId).remove(); //remove respective tab content

  if(tabToShow){
    currentTab = tabToShow;
    currentTab.tab('show');
  }
}

//shows the tab with passed content div id..paramter tabid indicates the div where the content resides
function showTab(tabId) {
    $('#dashboard-tab a').not('#addTab').each(function(){
        var tabContentId = $(this).attr("href");
        $(tabContentId).removeClass('active');
        $(tabContentId).addClass('hide');
    });
    currentTab = $('#dashboard-tab a[id='+ tabId +']');
    var tabContentId = $('#dashboard-tab a[href="#' + tabId + '-content"]').tab('show');
}
//return current active tab
function getCurrentTab() {
    return currentTab;
}

// This function will create a new tab here and it will load the url content in tab content div.
function createNewTabAndLoadUrl(url, params, loadDivSelector) {

    $('#loading-spinner').show();

    var data = {};

    // console.log(url + ' ' + params + ' ' + loadDivSelector);

    $.each(params, function(param){
      console.log(params);
      data[param] = params[param];
    });

    $.ajax({
          type     : "GET",
          url      : url,
          data     : data,
          success  : function(data)
          {
              $("" + loadDivSelector).html(data);
              console.log($("" + loadDivSelector));
              $('#loading-spinner').hide();
              $("" + loadDivSelector).find('.tab-pane').on('content:reload', function(e, cb){
                  cb(this);
              });
              $("" + loadDivSelector).on('content:reload', function(e, cb){
                  cb(this);
              });
          },
          error: function(){
              addMessage('Failed to load content.', 'warning');
              $('#loading-spinner').hide();
          },
    });

}

//this will return element from current tab
//example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
//to take care this situation we need get the element from current tab.
function getElement(selector) {
    var tabContentId = $currentTab.attr("href");
    return $("" + tabContentId).find("" + selector);
}


function removeCurrentTab() {
    var tabContentId = $currentTab.attr("href");
    $currentTab.parent().remove(); //remove li of tab
    $('#dashboard-tab a:last').tab('show'); // Select first tab
    $(tabContentId).remove(); //remove respective tab content
}

function clearAllTabs(){
    $('#dashboard-tab a').not('#addTab , .app-workplaces , .app-admin').each(function(tab){
        var tabContentId = $(tab).attr("href");
        $(tab).parent().remove();
        $(tabContentId).remove();
    });
    delTab('*');
    workplaceTab = {
      id: $('.app-workplaces').attr('id'),
      url: CONST.TABREFS.APPS.workplaces.url,
      title: 'Workplaces',
      app: CONST.TABREFS.APPS.workplaces.name,
      appdata: '',
    };
    adminTab = {
      id: $('.app-admin').attr('id'),
      url: CONST.TABREFS.APPS.admin.url,
      title: 'Administration',
      app: CONST.TABREFS.APPS.admin.name,
      appdata: '',
    };
    if (adminTab.id != undefined) {
        if (adminTab.id != currentTab.attr('id')) {
            showTab(adminTab.id);
        }
        saveTab(adminTab.title, adminTab.url, adminTab.app, adminTab.appdata, adminTab.id);
    }
    else if (workplaceTab.id != undefined) {
        showTab(workplaceTab.id);
    }
}

function addTab(title, url, app = '', appdata = '', save = true, pos = ''){

    if(pos == ''){
      var tabId = '' //this is id on tab content div where the
      do{
        tabCount = tabCount + 1;
        tabId = "tab" + tabCount;
      }while(usedIds.includes(tabId));
    }else{
      var tabId = pos;
    }

    usedIds.push(tabId);

    if(save){
      saveTab(title, url, app, appdata, tabId);
    }

    if(app != ''){
      app = 'app-'+app;
    }else{
      app = 'app-apps';
    }

    $('#dashboard-tab li:last').before(
      `<li class="nav-item">
          <a id="` + tabId + `" class="nav-link rounded-0 `+ app +`" href="#` + tabId + `-content">
          ` + title + `
          <button type="button" class="close float-none closeTab" type="button">
          <span class="ml-2" >&times;</span></button></a>
      </li>`
    );
    $('#dashboard-tab-content').append(
      '<div id="' + tabId + '-content" class="tab-pane fade in h-100"></div>'
    );

    createNewTabAndLoadUrl(url, appdata, "#" + tabId + "-content");
    showTab(tabId);
    registerCloseEvent();

    $(this).blur();
}

function addTabWithCheck(title, url, app = ''){

    if($('.app-'+app).length){
        $('.app-'+app).tab('show');
    }else{
        addTab(title,url,app);
    }
}

function saveTab(title, url, app, appdata, id){
  $('#loading-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/tabs/save',
        data     : {
          'title':title,
          'url':url,
          'app':app,
          'appdata':appdata,
          'id':id,
        },
        cache    : true,
        success  : function(data)
        {
            $('#loading-spinner').hide();
        },
        error: function(){
            addMessage('Failed to save tab.', 'warning');
            $('#loading-spinner').hide();
        },
  });
}

function delTab(id){
  $('#loading-spinner').show();

  $.ajax({
        type     : "POST",
        url      : '/ajax/tabs/del',
        data     : {'id':id},
        cache    : true,
        success  : function(data)
        {
            $('#loading-spinner').hide();
        },
        error: function(){
            addMessage('Failed to delete saved tab.', 'warning');
            $('#loading-spinner').hide();
        },
  });
}

function loadTabs(){
  $('#loading-spinner').show();

  $.ajax({
        type     : "GET",
        url      : '/ajax/tabs/load',
        cache    : true,
        dataType : 'json',
        success  : function(data)
        {
            if(Object.keys(data).length > 0){
              $.each(data['tabs'], function(tab) {
                addTab(data['tabs'][tab]['title'],data['tabs'][tab]['url'],data['tabs'][tab]['app'],data['tabs'][tab]['appdata'],false,tab);
              });
              if(data['_current_tab'] != undefined){
                showTab(data['_current_tab']);
              }
            }else{
              addTab('Workplaces','/ajax/wp', 'workplaces');
            }
            $('#loading-spinner').hide();
        },
        error: function(){
            addMessage('Failed to load tabs.', 'warning');
            addTab('Workplaces','/ajax/wp', 'workplaces');
            $('#loading-spinner').hide();
        },
  });
}

function reloadTabAction(tab){
  var classes = $(tab).find('a').attr('class');
  var tabId = $(tab).find('a').attr('href');
  var appId = classes.match(/(app-+(.(?!\s))+.)/g)[0];
  var url = null;
  switch(appId) {
    case "app-admin":
      url = '/ajax/admin';
      break;
    case "app-profile":
      url = '/ajax/profile';
      break;
    case "app-workplaces":
      url = '/ajax/wp';
      break;
    case "app-apps":
      url = '/ajax/apps';
      break;
  }
  if (url != null && tabId != undefined) {
    refreshTab(tabId,url);
  }
}

function refreshTab(tabId, url){
  $('#loading-spinner').show();

  console.log(tabId+'+'+url);

  $("" + tabId).hide();

  var data = {};

  $.ajax({
        type     : "GET",
        url      : url,
        data     : data,
        success  : function(data)
        {
            $("" + tabId).show();
            $("" + tabId).removeAttr('style');
            animateCSS("" + tabId,'fadeIn');
            $("" + tabId).html(data);
            $('#loading-spinner').hide();
        },
        error: function(){
            addMessage('Failed to refresh content.', 'warning');
            $('#loading-spinner').hide();
        },
  });
}
