/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-05-22T10:53:31+02:00
 * @Project: OGDT
 * @Filename: adminPanel.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-09T18:09:59+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const OBJECT_TYPE_USER = 'user';
const OBJECT_TYPE_WORKPLACE = 'workplace';
const OBJECT_TYPE_WORKGROUP = 'workgroup';

const FETCH_PROFILE_URL = '/api/profile/';
const FETCH_USER_URL = '/api/user/';
const FETCH_WORKPLACE_URL = '/api/workplace/';
const FETCH_WORKGROUP_URL = '/api/workgroup/';

$("#admin-tab a").on('show.bs.tab', function (e) {
  var tab = $(this).attr('href');
  var url = '';

  console.log($(".modal.show"));

  $(".modal.show").modal('hide');

  switch(tab) {
    case "#admin_global_settings":
      url = '/ajax/admin/global_settings';
      break;
    case "#admin_security":
      url = '/ajax/admin/security';
      break;
    case "#admin_users":
      url = '/ajax/admin/users';
      break;
    case "#admin_permissions_groups":
      url = '/ajax/admin/permissions_groups';
      break;
    case "#admin_workplaces":
      url = '/ajax/admin/workplaces';
      break;
    case "#admin_clusters":
      url = '/ajax/admin/clusters';
      break;
    case "#admin_workgroups":
      url = '/ajax/admin/workgroups';
      break;
    case "#admin_projects":
      url = '/ajax/admin/projects';
      break;
    case "#admin_ressources":
      url = '/ajax/admin/ressources';
      break;
    case "#admin_applications":
      url = '/ajax/admin/applications';
      break;
  }

  if(url != ''){
    refreshTab(tab, url);
  }

});

// USERS
function usersModalShow(type, item, element){
  var modalId = null;
  var action = null;
  var error = false;
  // Case with select box
  if (type == 'object') {
    action = item.value
    $(item).val('default').selectpicker('refresh');
  }
  if (type == 'string') {
    action = item
  }
  switch (action) {
    case 'delete':
      modalId = 'userDataTableDelete';
      userId = $(element).parent().attr('id').split('_')[1];
      username = $('#username_'+userId).text();
      updateConfirmModal("usersManagement(this,'delete',"+userId+")",username,modalId);
      break;
    case 'deleteM':
      modalId = 'userDataTableDeleteM';
      updateConfirmModal("usersManagement(this,'deleteM')",null,modalId);
      break;
    case 'enable':
      modalId = 'userDataTableEnable';
      userId = $(element).parent().attr('id').split('_')[1];
      username = $('#username_'+userId).text();
      updateConfirmModal("usersManagement(this,'enable',"+userId+")",username,modalId);
      break;
    case 'enableM':
      modalId = 'userDataTableEnableM';
      updateConfirmModal("usersManagement(this,'enableM')",null,modalId);
      break;
    case 'disable':
      modalId = 'userDataTableDisable';
      userId = $(element).parent().attr('id').split('_')[1];
      username = $('#username_'+userId).text();
      updateConfirmModal("usersManagement(this,'disable',"+userId+")",username,modalId);
      break;
    case 'disableM':
      modalId = 'userDataTableDisableM';
      updateConfirmModal("usersManagement(this,'disableM')",null,modalId);
      break;
    case 'add':
      modalId = 'userDataTableAdd';
      break;
    case 'edit':
      userId = $(element).parent().attr('id').split('_')[1];
      modalId = 'userDataTableEdit';
      error = updateEditModal(userId,modalId,OBJECT_TYPE_USER);
      break;
    default:
      break;
  }

  if(modalId != null && error != true && $('#'+modalId).hasClass('cancel-show') != true){
    showCustomModal(modalId);
  }else{
    $('#'+modalId).removeClass('cancel-show');
  }
}

// APPS
function applicationsModalShow(sel){
  var modalId = null;
  switch (sel.value) {
    case 'uninstall':
      modalId = 'applicationDataTableUninstall';
      break;
    case 'enable':
      modalId = 'applicationDataTableEnable';
      break;
    case 'disable':
      modalId = 'applicationDataTableDisable';
      break;
    default:
      break;
  }

  if(modalId != null){
    $('#'+modalId).modal();
  }
}

// CLUSTERS
function clustersModalShow(sel){
  var modalId = null;
  switch (sel.value) {
    case 'delete':
      modalId = 'clusterDataTableUninstall';
      break;
    case 'enable':
      modalId = 'clusterDataTableEnable';
      break;
    case 'disable':
      modalId = 'clusterDataTableDisable';
      break;
    default:
      break;
  }

  if(modalId != null){
    $('#'+modalId).modal();
  }
}

// PERMISSIONSGROUPS
function permissionsGroupsModalShow(sel){
  var modalId = null;
  switch (sel.value) {
    case 'delete':
      modalId = 'permissionsGroupDataTableUninstall';
      break;
    case 'enable':
      modalId = 'permissionsGroupDataTableEnable';
      break;
    case 'disable':
      modalId = 'permissionsGroupDataTableDisable';
      break;
    default:
      break;
  }

  if(modalId != null){
    $('#'+modalId).modal();
  }
}

// RESSOURCES
function ressourcesModalShow(sel){
  var modalId = null;
  switch (sel.value) {
    case 'delete':
      modalId = 'ressourceDataTableUninstall';
      break;
    case 'enable':
      modalId = 'ressourceDataTableEnable';
      break;
    case 'disable':
      modalId = 'ressourceDataTableDisable';
      break;
    default:
      break;
  }

  if(modalId != null){
    $('#'+modalId).modal();
  }
}

// PROJECTS
function projectsModalShow(sel){
  var modalId = null;
  switch (sel.value) {
    case 'delete':
      modalId = 'projectDataTableUninstall';
      break;
    case 'enable':
      modalId = 'projectDataTableEnable';
      break;
    case 'disable':
      modalId = 'projectDataTableDisable';
      break;
    default:
      break;
  }

  if(modalId != null){
    $('#'+modalId).modal();
  }
}

// WORKGROUPS
function workgroupsModalShow(type, item, element){
  var modalId = null;
  var action = null;
  // Case with select box
  if (type == 'object') {
    action = item.value
    $(item).val('default').selectpicker('refresh');
  }
  if (type == 'string') {
    action = item
  }
  switch (action) {
    case 'add':
      modalId = 'workgroupsDataTableAdd';
      break;
    case 'edit':
      modalId = 'workgroupsDataTableEdit';
      workgroupId = $(element).parent().attr('id').split('_')[1];
      error = updateEditModal(workgroupId,modalId,OBJECT_TYPE_WORKGROUP);
      break;
    case 'delete':
      modalId = 'workgroupsDataTableDelete';
      workgroupId = $(element).parent().attr('id').split('_')[1];
      workgroupLabel = $('#label_'+workgroupId).text();
      updateConfirmModal("workgroupsManagement(this,'delete',"+workgroupId+")",workgroupLabel,modalId);
      break;
    case 'deleteM':
      modalId = 'workgroupsDataTableDeleteM';
      updateConfirmModal("workgroupsManagement(this,'deleteM')",null,modalId);
      break;
    default:
      break;
  }

  if(modalId != null){
    showCustomModal(modalId);
  }
}

// WORKPLACES
function workplacesModalShow(type, item, element){
  var modalId = null;
  var action = null;
  // Case with select box
  if (type == 'object') {
    action = item.value
    $(item).val('default').selectpicker('refresh');
  }
  if (type == 'string') {
    action = item
  }
  switch (action) {
    case 'add':
      modalId = 'workplacesDataTableAdd';
      break;
    case 'edit':
      modalId = 'workplacesDataTableEdit';
      workplaceId = $(element).parent().attr('id').split('_')[1];
      error = updateEditModal(workplaceId,modalId,OBJECT_TYPE_WORKPLACE);
      break;
    case 'delete':
      modalId = 'workplacesDataTableDelete';
      workplaceId = $(element).parent().attr('id').split('_')[1];
      workplaceLabel = $('#label_'+workplaceId).text();
      updateConfirmModal("workplacesManagement(this,'delete',"+workplaceId+")",workplaceLabel,modalId);
      break;
    case 'deleteM':
      modalId = 'workplacesDataTableDeleteM';
      updateConfirmModal("workplacesManagement(this,'deleteM')",null,modalId);
      break;
    default:
      break;
  }

  if(modalId != null){
    showCustomModal(modalId);
  }
}

function updateConfirmModal(opMethod,display,modalId){
  if (display != null){
    $('#'+modalId).find('.modal-display').text(display);
  }
  $('#'+modalId).find('.confirm-button').attr('onclick',opMethod);
  $('#'+modalId).find('.confirm-button').attr('disabled',false);
}

function updateEditModal(id,modalId,objectType){

  // Prepare Ajax request depending on type of data to fetch
  switch (objectType) {
    case OBJECT_TYPE_USER:
      opMethod = "usersManagement(this, 'edit', "+id+");";
      fetchUrl = FETCH_PROFILE_URL+id;
      successCb = function(profile)
        {
          fetchUrl = FETCH_USER_URL+profile['user'];
          $.ajax({
              type     : "GET",
              url      : fetchUrl,
              success  : function(user)
              {
                  $('#'+modalId).find('.usersManagement_username').attr('placeholder',user['username']);
                  $('#'+modalId).find('.editModal_title_username').text(user['username']);
                  $('#loading-spinner').hide();
              },
              error    : function()
              {
                  $('#'+modalId).modal('hide');
                  addMessage('Failed to retrieve user details.','danger');
                  $('#loading-spinner').hide();
              }
          })
          $('#'+modalId).find('.usersManagement_firstname').attr('placeholder',profile['firstname']);
          $('#'+modalId).find('.usersManagement_lastname').attr('placeholder',profile['lastname']);
          $('#'+modalId).find('.usersManagement_mail').attr('placeholder',profile['email']);

        };
      errorCb = function()
        {
          $('#'+modalId).modal('hide');
          message = 'Failed to retrieve profile details.'
          addMessage(message,'danger');
          $('#'+modalId).addClass('cancel-show');
          $('#loading-spinner').hide();
        };
      break;
    case OBJECT_TYPE_WORKPLACE:
      opMethod = "workplacesManagement(this, 'edit', "+id+");";
      fetchUrl = FETCH_WORKPLACE_URL+id;
      successCb = function(workplace)
        {
          $('#'+modalId).find('.editModal_title_label').text(workplace['label']);
          $('#'+modalId).find('.workplacesManagement_label').attr('placeholder',workplace['label']);
          $('#'+modalId).find('.workplacesManagement_urlPrefix').attr('placeholder',workplace['url_prefix']);

          var ownerTypesList = {};

          $('#'+modalId).find('.workplacesManagement_ownerType option').each( (index, option) => {
            if (option.value == workplace['owner_type']) {
              $(option).attr('selected',true);
            } else {
              $(option).attr('selected',false);
            }
            ownerTypesList[option.value] = index;
          });

          var timeZonesList = {};

          $('#'+modalId).find('.workplacesManagement_timeZone option').each( (index, option) => {
            if (option.value == workplace['time_zone']) {
              $(option).attr('selected',true);
            } else {
              $(option).attr('selected',false);
            }
            timeZonesList[option.value] = index;
          });

          $('#loading-spinner').hide();
        };
      errorCb = function()
        {
          $('#'+modalId).modal('hide');
          addMessage('Failed to retrieve workplace details.','danger');
          $('#'+modalId).addClass('cancel-show');
          $('#loading-spinner').hide();
        };
      break;
    case OBJECT_TYPE_WORKGROUP:
      opMethod = "workgroupsManagement(this, 'edit', "+id+");";
      fetchUrl = FETCH_WORKGROUP_URL+id;
      console.log(fetchUrl);
      successCb = function(workgroup)
        {
          $('#'+modalId).find('.editModal_title_label').text(workgroup.label);
          $('#'+modalId).find('.workgroupsManagement_label').attr('placeholder',workgroup.label);

          var workplacesList = {};

          $.ajax({
                type     : "GET",
                url      : FETCH_WORKPLACE_URL+workgroup.workplace,
                success  : function(workplace){
                  workgroup.workplace = workplace.label;
                  $('#'+modalId).find('.workgroupsManagement_workplace').append(new Option(workgroup.workplace, workgroup.workplace, true, true));

                  $('#'+modalId).find('.workgroupsManagement_workplace option').each( (index, option) => {
                    if (option.value == workgroup['workplace']) {
                      $(option).attr('selected',true);
                    } else {
                      $(option).attr('selected',false);
                    }
                    workplacesList[option.value] = index;
                  });
                },
                error    : function(){
                  $('#'+modalId).modal('hide');
                  addMessage('Failed to retrieve workgroup details.','danger');
                  $('#'+modalId).addClass('cancel-show');
                  $('#loading-spinner').hide();
                },
          });

          $('#'+modalId).find('.workgroupsManagement_workplace').attr('disabled',true);

          $('#loading-spinner').hide();
        };
      errorCb = function()
        {
          $('#'+modalId).modal('hide');
          addMessage('Failed to retrieve workgroup details.','danger');
          $('#'+modalId).addClass('cancel-show');
          $('#loading-spinner').hide();
        };
      break;
    default:
      addMessage('Aucune correspondance de type pour la mise à jour du Modal','danger');
      return true;
  }

  // Set operational method for modal's submit button
  $('#'+modalId).find('.submit_button').attr('onclick',opMethod);

  // Execute Ajax modal update
  $('#loading-spinner').show();
  $.ajax({
        type     : "GET",
        url      : fetchUrl,
        success  : successCb,
        error    : errorCb,
  });

  // No error
  return false;
}
