/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-10T01:31:46+02:00
 * @Project: OGDT
 * @Filename: const.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T06:45:09+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

// CONSTANTS


const CONST =
{
  Clipboard: navigator.clipboard,
  EMAIL_REGEX: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
  URLS:
  {
    EXIST_URL: '/ajax/objects/check',
    UTILS:
    {
      USER:
      {
        ADD: '/ajax/admin/users/add',
        EDIT: '/ajax/admin/users/edit',
        DEL: '/ajax/admin/users/del',
        CHANGE_STATUS: '/ajax/admin/users/status',
      },
      WORKGROUP:
      {
        ADD: '/ajax/admin/workgroups/add',
        EDIT: '/ajax/admin/workgroups/edit',
        DEL: '/ajax/admin/workgroups/del',
      },
      WORKPLACE:
      {
        ADD: '/ajax/admin/workplaces/add',
        EDIT: '/ajax/admin/workplaces/edit',
        DEL: '/ajax/admin/workplaces/del',
      },
    },
  },
  TABREFS:
  {
    APPS:
    {
      admin:
      {
        name: 'admin',
        url: '/ajax/admin',
      },
      workplaces:
      {
        name: 'workspaces',
        url: '/ajax/wp',
      },
    },
    ADMIN:
    [
      {
        name: 'admin_global_settings',
        url: '/ajax/admin/global_settings',
      },
      {
        name: 'admin_security',
        url: '/ajax/admin/security',
      },
      {
        name: 'admin_users',
        url: '/ajax/admin/users',
      },
      {
        name: 'admin_permissions_groups',
        url: '/ajax/admin/permissions_groups',
      },
      {
        name: 'admin_workplaces',
        url: '/ajax/admin/workplaces',
      },
      {
        name: 'admin_clusters',
        url: '/ajax/admin/clusters',
      },
      {
        name: 'admin_workgroups',
        url: '/ajax/admin/workgroups',
      },
      {
        name: 'admin_projects',
        url: '/ajax/admin/projects',
      },
      {
        name: 'admin_ressources',
        url: '/ajax/admin/ressources',
      },
      {
        name: 'admin_applications',
        url: '/ajax/admin/applications',
      },
    ],
  },
};
