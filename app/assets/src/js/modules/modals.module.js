/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-10T02:39:48+02:00
 * @Project: OGDT
 * @Filename: modals.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T02:39:52+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

$('.modal').on('show.bs.modal', function (e) {
var button = e.relatedTarget;
if($(button).hasClass('no-modal')) {
  e.stopPropagation();
}
$(button).removeClass('no-modal');
});

// Custom BS modal
function showCustomModal(modalId){
$('#'+modalId).modal('show');
if ($('#'+modalId).hasClass('formModal')) {
  $('#'+modalId).find('form').each(function(){
    this.reset();
    resetFormValidation(this);
  });
}

//appending modal background inside the right div
$('.modal-backdrop').appendTo($('#'+modalId).parent());
$('.modal-backdrop').css({'width': '100%', 'height': '100%'});

//remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
$('body').removeClass("modal-open")
$('body').css("padding-right","");
}
