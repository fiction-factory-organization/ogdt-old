/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-10T02:32:18+02:00
 * @Project: OGDT
 * @Filename: messages.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T02:35:23+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

// messages
function addMessage(message, level){
 $('#messages_handler').append(
     `<div class="row m-3 p-0 animated alert bounce fade show position-relative fixed-bottom rounded-0 message-alert alert-`+ level +`" role="alert">
     <div class="col col-11 pt-3 pb-3">
     ` + message + `
     </div>
     <div class="col col-1">
       <button type="button" class="btn close" data-dismiss="alert" aria-label="Close">
           <span style="font-size: 16px;" aria-hidden="true">&times;</span>
       </button>
     </div>
     </div>`
 )
 // Alerts auto dismiss
 $("#messages_handler > div:last").delay(4000).fadeOut(function() {
     $(this).remove();
 });
}
