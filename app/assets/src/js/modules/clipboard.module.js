/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-10T02:32:55+02:00
 * @Project: OGDT
 * @Filename: clipboard.module.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T02:54:14+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

// CONST.Clipboard Write
function setClipboardData(text,dataName = '') {
 if (CONST.Clipboard == undefined) {
   addMessage('Failed to write to clipboard','danger');
 } else {
   CONST.Clipboard.writeText(text).then(
     function() {
       addMessage(dataName+' copied to clipboard','success');
     },
     function() {
       addMessage('Failed to write to clipboard','danger');
     }
   );
 }
}
